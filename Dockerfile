# Etapa de compilación
FROM node:latest as build

# Establecer directorio de trabajo
WORKDIR /app

# Copiar el archivo de configuración de Angular
COPY package.json package-lock.json ./

# Instalar dependencias
RUN npm install

# Copiar el resto de la aplicación
COPY . .

# Compilar la aplicación Angular en producción
RUN npm run build

# Etapa de producción
FROM nginx:alpine

# Copiar los archivos compilados de Angular al directorio de NGINX
COPY --from=build /app/dist/pulsoverde /usr/share/nginx/html

# Copiar configuración personalizada de NGINX
COPY nginx-custom.conf /etc/nginx/conf.d/default.conf

# Env config
COPY --from=build /app/start.sh /usr/share/nginx/html
RUN chmod +x /usr/share/nginx/html/start.sh

# Exponer el puerto 80
EXPOSE 80

CMD ["/bin/sh", "-c", "/usr/share/nginx/html/start.sh"]
