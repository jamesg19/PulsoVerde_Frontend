const express = require('express');
const cors = require('cors');
const { createProxyMiddleware } = require('http-proxy-middleware');
const compression = require('compression'); // Importa el paquete de compresión

const app = express();


// Habilitar CORS para todas las rutas
/*
app.use(cors({
  origin: 'http://ec2-100-25-164-18.compute-1.amazonaws.com:8024', // Cambia esto según sea necesario
  methods: ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
  allowedHeaders: ['Content-Type', 'Authorization']
}));*/

// Habilita la compresión antes de las rutas y las solicitudes de proxy
app.use(compression());

app.use(express.static('./dist/pulsoverde/'));

app.get('/*', (req, res) =>
  res.sendFile('index.html', { root: 'dist/pulsoverde/' })
);

app.listen( 4200, () => {
  console.log('Server is running on port 4200');
});
