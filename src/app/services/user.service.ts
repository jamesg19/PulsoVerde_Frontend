import {Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from "@angular/common/http";
import { Observable } from "rxjs";
import { UserDto } from "../Objects/UserDto";
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl = `${this.environment.HTL_BACKEND_URL}/v1/users`;

  constructor(
    private http: HttpClient,
    private environment:EnvironmentService,
  ) {}

  getMyInfo(): Observable<UserDto> {
    return this.http.get<UserDto>(`${this.apiUrl}/me`);
  }

  findByQuery(params: Map<string, any>): Observable<UserDto[]> {
    let httpParams = new HttpParams();
    params.forEach((value, key, map) => {
      httpParams = httpParams.set(key, value);
    });
    return this.http.get<UserDto[]>(`${this.apiUrl}/find-by-query`, { params: httpParams });
  }

  findByQueryAndPagination(user, size = 4, page = 0): Observable<HttpResponse<any>> {
    // Make the HTTP GET request with headers and observe the response with full headers
    return this.http.get<any>(`${this.apiUrl}/find-by-query`, {
      observe: 'response', params: {
        user: user,
        size: size,
        page: page
      }
    });
  }

}

