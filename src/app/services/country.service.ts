import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CountryDto} from "../Objects/CountryDto";
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  private apiUrl = `${this.environment.HTL_BACKEND_URL}/v1/country`;

  constructor(
    private http: HttpClient,
    private environment:EnvironmentService,
  ) {}

  getCountry(): Observable<CountryDto[]> {
    return this.http.get<CountryDto[]>(`${this.apiUrl}`);
  }

}
