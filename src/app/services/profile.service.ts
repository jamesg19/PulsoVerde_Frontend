import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {UserDto} from "../Objects/UserDto";
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private apiUrl = `${this.environment.HTL_BACKEND_URL}/v1/users`;

  constructor(
    private http: HttpClient,
    private environment:EnvironmentService,
  ) {}

  getUserInfo(): Observable<UserDto> {
    return this.http.get<UserDto>(`${this.apiUrl}/me`);
  }

  updateUser(user: UserDto): Observable<UserDto> {
    return this.http.put<UserDto>(`${this.apiUrl}/edit`, user);
  }

  seeOtherUser(idUser:any):Observable<UserDto>{
    return this.http.get<UserDto>(`${this.apiUrl}/find-by-id/`+idUser);
  }

}

