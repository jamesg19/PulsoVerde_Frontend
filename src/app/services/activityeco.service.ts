import {Injectable} from '@angular/core';
import {HttpClient,HttpHeaders } from "@angular/common/http";
import {Observable} from "rxjs";
import {ActivityEcoDto} from "../Objects/ActivityEcoDto";
import { ActivityEcoUpRequest, AssignedActivityRequest } from '../models/activity.model';
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class ActivityEcoService {

  private apiUrl = `${this.environment.HTL_BACKEND_URL_ACTIVITY}/v1`;

  constructor(
    private http: HttpClient,
    private environment:EnvironmentService,
  ) { }

  setactivityIfo(data: ActivityEcoUpRequest){
    return this.http.post(this.apiUrl+ `/activity`, data);
  }

  updateactivityIfo(acativity: ActivityEcoUpRequest,token: string, idActivity: string){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    return this.http.put(`${this.apiUrl}/activity/${idActivity}`, acativity, { headers });
  }

  ActivateactivityIfo(token: string, idActivity: string){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    return this.http.put(`${this.apiUrl}/activity/${idActivity}`, { headers });
  }



  getActivityInfo(state: string): Observable<ActivityEcoDto> {
    return this.http.get<ActivityEcoDto>(`${this.apiUrl}/activity/findAll-by-state/`+state);
  }

  //------------------------------------------

  getActivityAssignInfo(): Observable<AssignedActivityRequest> {

    return this.http.get<AssignedActivityRequest>(`${this.apiUrl}/assigment`);
  }

  getActivityUnAssignInfo(): Observable<AssignedActivityRequest> {

    return this.http.get<AssignedActivityRequest>(`${this.apiUrl}/activity/findAll-not-assigment`);
  }

  AsignateactivityIfo(token: string, idActivity: string){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    return this.http.post(`${this.apiUrl}/assigment/${idActivity}`, { headers });
  }
  unAsignateactivityIfo(token: string, idActivity: string){
    const headers = new HttpHeaders({
      'Authorization': `Bearer ${token}`
    });
    return this.http.delete(`${this.apiUrl}/assigment/${idActivity}`, { headers });
  }



}

