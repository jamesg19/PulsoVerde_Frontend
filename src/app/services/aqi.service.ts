import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { ActivityEcoDto } from "../Objects/ActivityEcoDto";
import { ActivityEcoUpRequest, AssignedActivityRequest } from '../models/activity.model';
import { EnvironmentService } from "./environment.service";

@Injectable({
	providedIn: 'root'
})
export class AQIService {

	private URL = `${this.environment.HTL_BACKEND_URL_AIR}/v1/aq`;

	constructor(
		private http: HttpClient,
		private environment: EnvironmentService,
	) { }

	here() {
		return this.http.get(this.URL + "/here");
	}

	specific(lat:number, lon: number){
		return this.http.get(this.URL + "/specific", {
			params: {
				latitude: lat,
				longitude: lon
			}
		})
	}

	city(cityId: string){
		return this.http.get(this.URL + "/city", {
			params: {
				cityId: cityId,
			}
		})
	}

	ranking() {
		return this.http.get(this.URL + "/ranking");
	}
}