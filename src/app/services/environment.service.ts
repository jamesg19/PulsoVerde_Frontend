import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EnvironmentService {

  private configUrl = 'assets/appsettings.json';

  HTL_BACKEND_URL = "";
  HTL_BACKEND_URL_ACTIVITY = "";
  HTL_BACKEND_URL_AIR = "";
  HTL_BACKEND_URL_REPORT = "";

  constructor(
    private http: HttpClient,
  ) {}

  initVariables(): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      this.http.get(this.configUrl).subscribe(
        (config: any) => {
          this.HTL_BACKEND_URL = `${config["HTL_BACKEND_URL"]}`;
          this.HTL_BACKEND_URL_ACTIVITY = `${config["HTL_BACKEND_URL_ACTIVITY"]}`;
          this.HTL_BACKEND_URL_AIR = `${config["HTL_BACKEND_URL_AIR"]}`;
          this.HTL_BACKEND_URL_REPORT = `${config["HTL_BACKEND_URL_REPORT"]}`;
          resolve();
        },
        error => {
          console.error('Error initializing environment variables', error);
          reject(error);
        }
      );
    });
  }

}
