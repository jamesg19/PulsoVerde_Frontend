import {Injectable} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthRequest, ChangePassRequest, SignUpRequest } from '../models/auth.model';
import { jwtDecode } from 'jwt-decode';
import {EnvironmentService} from "./environment.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private URL = `${this.environment.HTL_BACKEND_URL}/v1/auth`;

  constructor(
    private http: HttpClient,
    private environment:EnvironmentService,
  ) {}

  login(data: AuthRequest) {
    return this.http.post(this.URL + `/sign-in`, data);
  }

  signUp(signUp: SignUpRequest) {
    return this.http.post(this.URL + "/sign-up", signUp);
  }

  changePass(request: ChangePassRequest) {
    return this.http.put(this.URL + "/change-pass", request);
  }

  setToken(token: string) {
    localStorage.setItem('jwt', token);
  }

  getToken() {
    return localStorage.getItem('jwt');
  }

  logout() {
    localStorage.clear();
  }

  getRol() {
    var token = this.getToken();
    try {
      const decodedToken = jwtDecode(token);
      return decodedToken['roles'];
    } catch (error) {
      console.error('Error decoding token:', error);
      return null;
    }
  }
}
