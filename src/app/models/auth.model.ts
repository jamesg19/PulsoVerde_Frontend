export class AuthRequest {
	email: string;
	password: string;
}

export class SignUpRequest {
	firstName: string;
	lastName: string;
	birthday: any;
	password: string;
	email: string;
	age: number;
	countryId: number;
	description: string;
	profilePhoto: string;
	username: string;
}

export class AuthResponse {
	jwt: string;
}

export class ChangePassRequest {
	token: string;
	newPassword: string;
}