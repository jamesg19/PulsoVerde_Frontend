export interface AQIResponse {
    status: string;
    aqi: number;
    idx: number;
    latitude: number;
    longitude: number;
    name: string;
    url: string;
    location: string;
    time: any;
    original: AqicnResponseDto;
}

export interface AQIRankingResponse {
    aqi: number;
    country: string;
    latitude: number;
    longitude: number;
}

export interface AqicnResponseDto {
    status: string;
    data: Data;
}

export interface Data {
    aqi: number;
    idx: number;
    city: City;
    dominentpol: string;
    iaqi: Iaqi;
    time: TimeAQI;
}

export interface TimeAQI {
    iso: string;
    s: string;
    tz: string;
    v: number;
}

export interface City {
    geo: number[];
    name: string;
    url: string;
    location: string;
}

export interface Iaqi {
    co: Value;
    h: Value;
    no2: Value;
    o3: Value;
    p: Value;
    pm10: Value;
    pm25: Value;
    so2: Value;
    t: Value;
    w: Value;
}

export interface Value {
    v: number;
}