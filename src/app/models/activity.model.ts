

export class ActivityEcoUpRequest {
	id: number;
	name: string;
	description: string;
	state: boolean;
	publicationExpired: any;
	publicationDate: any;
	activityPhoto: string;
}

export class ActivityEcoUpPut {
	name: string;
	description: string;
	publicationExpired: any;
	state: boolean;
	activityPhoto: string;
}

export class ChangeActivityRequest {
	token: string;
	activity: ActivityEcoUpPut;
}

export class ActivityAsigned{
	id: number;
	name: string;
    description: string;
    dateExpiration: any;
    userCreate: string;
    activityPhoto: string;
    state: boolean;
}

export class AssignedActivityRequest{
	id: number;
	dateAssigment: any;
    activity: ActivityAsigned;
}
