export class PaginationModel {
	pageIndex: number = 1;
	pageSize: number = 5;
	startElement: number = 0;
	lastElement: number;
    total: number = 0;
	constructor(
		pageIndex:number,
		pageSize:number,
		startElement:number,
		lastElement:number,
		total
	){
		this.pageIndex = pageIndex;
		this.pageSize = pageSize;
		this.startElement = startElement;
		this.lastElement = lastElement;
		this.total = total;
	}
}