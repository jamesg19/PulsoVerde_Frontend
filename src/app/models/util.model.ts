export class Pagination {
  total: any[] = [];
  filter: any[] = [];
  pageSize: number = 5;
  startElement: number = 0;
  lastElement: number = 0;
  pageIndex: number = 0;
}
