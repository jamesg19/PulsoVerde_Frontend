import { CommonModule, NgOptimizedImage } from '@angular/common';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgSelectModule } from '@ng-select/ng-select';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/layouts/sidebar/sidebar.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/layouts/header/header.component';
import { PaginationComponent } from './components/elements/pagination/pagination.component';
import { FormsModule } from '@angular/forms';
import { AuthInterceptor } from './security/auth.interceptor';
import { LoginComponent } from './components/pages/auth/login/login.component';
import { RegisterComponent } from './components/pages/auth/register/register.component';
import { AuthComponent } from './components/pages/auth/auth.component';
import { ToastrModule } from 'ngx-toastr';
import { SimpleModalComponent } from './components/modals/simple-modal/simple-modal.component';
import { LoadingButtonComponent } from './components/elements/loading-button/loading-button.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTabsModule } from '@angular/material/tabs';
import { InputAutocompleteComponent } from './components/elements/input-autocomplete/input-autocomplete.component';
import { FooterComponent } from './components/elements/footer/footer.component';
import { CheckRoleDirective } from 'src/app/security/check-role.directive';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AqiComponent } from './components/pages/aqi/aqi.component';
import { MapComponent } from './components/pages/aqi/map/map.component';
import { ButtonpvComponent} from './components/elements/button-pv/button-pv.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { ChangePasswordComponent } from './components/pages/change-password/change-password.component';
import { UsersSearchComponent } from './components/pages/users-search/users-search.component';
import { HighlightDirective } from './util/highlight.directive';
import {ActivityEcoComponent} from './components/pages/activity-ecological/activity-eco/activity-eco.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import {ActivityEcoCrudComponent} from './components/pages/activity-ecological/activity-eco-crud/activity-eco-crud.component';
import {ActivityEcologicComponent} from './components/pages/activity-ecological/activity-ecological.component';
import {EnvironmentService} from "./services/environment.service";
import { ToolMapComponent } from './components/pages/aqi/tool-map/tool-map.component';
import { RankingComponent } from './components/pages/aqi/ranking/ranking.component';

export function initializeApp(environmentService: EnvironmentService) {
  return () => environmentService.initVariables();
}

import { ViewOtherComponent } from './components/pages/profile/view-other/view-other.component';
import { MatDialogModule } from '@angular/material/dialog';
@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    FooterComponent,
    PaginationComponent,
    LoginComponent,
    AuthComponent,
    SimpleModalComponent,
    LoadingButtonComponent,
    InputAutocompleteComponent,
    FooterComponent,
    CheckRoleDirective,
    AqiComponent,
    MapComponent,
    ButtonpvComponent,
    RegisterComponent,
    ProfileComponent,
    ChangePasswordComponent,
    UsersSearchComponent,
    HighlightDirective,
    ActivityEcoComponent,
    ActivityEcoCrudComponent,
    ActivityEcologicComponent,
    ToolMapComponent,
    RankingComponent,
    ActivityEcologicComponent,
    ViewOtherComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    MatTooltipModule,
    MatTabsModule,
    NgSelectModule,
    NgOptimizedImage,
    NgbModule,
    MatSlideToggleModule,
    MatDialogModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [EnvironmentService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
