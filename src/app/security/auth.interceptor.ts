import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpEvent, HttpInterceptor, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, catchError, throwError } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	constructor(private http: HttpClient, private router: Router, private toastService: ToastrService) { }

	intercept(
		request: HttpRequest<any>,
		next: HttpHandler
	): Observable<HttpEvent<any>> {
		const authToken = localStorage.getItem('jwt'); // Asegúrate de tener un nombre de clave adecuado
		if (request.url.includes('auth')) {
			return next.handle(request);
		} else {
			request = request.clone({
				setHeaders: {
					Authorization: `Bearer ` + authToken
				}
			});
			return next.handle(request).pipe(
				catchError((error: HttpErrorResponse) => {
					if (error.status === 401) {
						this.handleUnauthorized();
					}
					return throwError(error);
				})
			);
		}
	}

	private handleUnauthorized() {
		localStorage.clear();
		this.toastService.error("Session expired!");
		this.router.navigate(['auth/login']);
	}

}