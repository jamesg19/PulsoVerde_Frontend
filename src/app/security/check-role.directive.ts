import { Directive, ElementRef, Input, OnInit } from '@angular/core';
import { RoleEnum } from 'src/app/enums/role.enum';
import { AuthService } from '../services/auth.service';

@Directive({
  selector: '[appCheckRole]'
})
export class CheckRoleDirective implements OnInit {

  @Input() rol: RoleEnum = RoleEnum.USER;

  constructor(
    private elementRef: ElementRef,
    private authService: AuthService) {
  }

  ngOnInit(): void {
    const userRol = this.authService.getRol();
    if (userRol == this.rol) {
      return;
    }
    this.elementRef.nativeElement.remove();
  }
}
