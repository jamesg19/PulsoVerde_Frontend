import { CanActivateFn, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { inject } from '@angular/core';
import { RoleEnum } from '../enums/role.enum';

export const authGuard: CanActivateFn = (route, state) => {
  const router = new Router();
  const authService: AuthService = inject(AuthService);
  const token = authService.getToken();
  if (token) {
    const userRol = authService.getRol();
    const allowedRol = route.data['roles'];
    if (userRol) {
      if (state.url.includes('/auth/')) {
        void router.navigate(['aqi/map']);
        return false;
      } else {
        for (let i = 0; i < allowedRol.length; i++) {
          const element = allowedRol[i];
          if (userRol == element) {
            return true;
          }
        }
        void router.navigate(['aqi/map']);
        return false;
      }
    } else {
      authService.logout();
      void router.navigate(['auth/login']);
      return false;
    }
  } else {
    if (state.url.includes('/auth/')) {
      return true;
    } else {
      void router.navigate(['auth/login']);
      return false;
    }
  }
};
