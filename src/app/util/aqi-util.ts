import * as L from 'leaflet';
export class AQIUtil {
	static getCustomIcon(): L.Icon {
		return L.icon({
			iconUrl: 'assets/img/marker.png', // Path to your custom icon image
			iconSize: [30, 30], // Size of the icon
			iconAnchor: [15, 15], // Point of the icon which will correspond to marker's location
			popupAnchor: [0, 0] // Point from which the popup should open relative to the iconAnchor
		});
	}
}