import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/pages/auth/login/login.component';
import { AuthComponent } from './components/pages/auth/auth.component';
import { AqiComponent } from './components/pages/aqi/aqi.component';
import { MapComponent } from './components/pages/aqi/map/map.component';
import { authGuard } from './security/auth.guard';
import { RoleEnum } from './enums/role.enum';
import { RegisterComponent } from './components/pages/auth/register/register.component';
import { ProfileComponent } from "./components/pages/profile/profile.component";
import { ChangePasswordComponent } from './components/pages/change-password/change-password.component';
import {UsersSearchComponent} from "./components/pages/users-search/users-search.component";
import { ActivityEcoComponent } from './components/pages/activity-ecological/activity-eco/activity-eco.component';
import { ActivityEcoCrudComponent } from './components/pages/activity-ecological/activity-eco-crud/activity-eco-crud.component';
import {ActivityEcologicComponent} from './components/pages/activity-ecological/activity-ecological.component';
import { ToolMapComponent } from './components/pages/aqi/tool-map/tool-map.component';
import { RankingComponent } from './components/pages/aqi/ranking/ranking.component';
const routes: Routes = [
  { path: '', redirectTo: 'auth/login', pathMatch: 'full' },
  {
    path: 'auth', component: AuthComponent,
    canActivate: [authGuard],
    children: [
      { path: 'login', data: { breadcrumb: 'Login' }, component: LoginComponent },
      { path: 'register', data: { breadcrumb: 'Register' }, component: RegisterComponent }, // Nueva ruta para RegisterComponent
      { path: 'change-pass/:token', data: { breadcrumb: 'Change Password' }, component: ChangePasswordComponent }, // Nueva ruta para RegisterComponent
      {
        path: 'change-pass',
        redirectTo: 'login'
      }
    ]
  },
  {
    path: 'aqi', component: AqiComponent,
    canActivate: [authGuard],
    data: { roles: [RoleEnum.ADMIN, RoleEnum.USER] },
    children: [
      { path: 'map', data: { breadcrumb: 'Map' }, component: MapComponent },
      { path: 'tool', data: { breadcrumb: 'Tool' }, component: ToolMapComponent },
      { path: 'ranking', data: { breadcrumb: 'Ranking' }, component: RankingComponent }
    ]
  },
  {
    path: 'perfil', component: ProfileComponent,
    canActivate: [authGuard],
    data: { roles: [RoleEnum.ADMIN, RoleEnum.USER] },
  },
  {
    path: 'users',
    canActivate: [authGuard],
    data: { roles: [RoleEnum.ADMIN, RoleEnum.USER] },
    children: [
      { path: 'search', data: { breadcrumb: 'Search' }, component: UsersSearchComponent },
    ]
  },
  ,
  {
    path: 'activity', component:ActivityEcologicComponent,
    canActivate: [authGuard],
    data: { roles: [RoleEnum.ADMIN] },
    children: [
      { path: 'crud', component: ActivityEcoCrudComponent, data: { roles: [RoleEnum.ADMIN], breadcrumb: 'crud' }},
    ]
  },
  {
    path: 'activity', component:ActivityEcologicComponent,
    canActivate: [authGuard],
    data: { roles: [ RoleEnum.USER] },
    children: [
      { path: 'eco', component: ActivityEcoComponent, data: { roles: [RoleEnum.USER], breadcrumb: 'eco' }},
    ]
  }


  /*
  path: 'eco',
        component: ActivityEcoComponent,
        canActivate: [AuthGuard],
        data: { roles: [RoleEnum.USER], breadcrumb: 'eco' }
  */
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
