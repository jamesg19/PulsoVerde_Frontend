import { Component, OnDestroy, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AQIRankingResponse } from 'src/app/models/aqi.model';
import { AQIService } from 'src/app/services/aqi.service';
import * as L from 'leaflet';
import { AQIUtil } from 'src/app/util/aqi-util';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss']
})
export class RankingComponent implements OnInit, OnDestroy {
  loading = true;
  countries: AQIRankingResponse[] = [];
  private aqi = null;
  private markers: L.Marker[] = [];
  private map;
  longitude = 39.8282;
  latitude = -98.5795;

  constructor(
    private toastService: ToastrService,
    private aqiService: AQIService
  ) {

  }

  private initMap(): void {
    this.map = L.map('map', {
      center: [this.latitude, this.longitude],
      zoom: 1
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  ngOnInit(): void {
    this.aqiService.ranking().subscribe({
      next: (response) => {
        this.loading = false;
        this.countries = response as AQIRankingResponse[];
        this.latitude = this.countries[0].latitude;
        this.longitude = this.countries[0].longitude;
        this.initMap();
        var i = 0;
        this.countries.forEach((country) => {
          this.addCustomLabel(country, i < 5);
          i++;
        })
        this.markers[0].openPopup();
        this.map.panTo(new L.LatLng(this.latitude, this.longitude));
      },
      error: (error) => {
        this.loading = false;
        this.toastService.error("Error at get the ranking");
      }
    })
  }

  private addCustomLabel(rank: AQIRankingResponse, isAtTop): void {
    const latitude = rank.latitude;
    const longitude = rank.longitude;
    const aqi = rank.aqi;
    const country = rank.country;
    var icon = "";
    var color = "";
    var message = "";
    if (aqi < 51) {
      icon = "fa-circle-check";
      color = "#00ff37";
      message = "ICA Bueno";
    } else if (aqi >= 51 && aqi < 101) {
      icon = "fa-thumbs-up";
      color = "#f9ff00";
      message = "ICA Moderado";
    } else if (aqi >= 101 && aqi < 151) {
      icon = "fa-triangle-exclamation";
      color = "#ffc100";
      message = "ICA Desfavorable para grupos sensibles";
    } else if (aqi >= 151 && aqi < 201) {
      icon = "fa-circle-radiation";
      color = "#ff0000";
      message = "ICA Dañino a la salud";
    } else {
      icon = "fa-skull";
      color = "#7f00ff";
      message = "ICA Muy dañino a la salud";
    }
    rank['icon'] = icon;
    rank['color'] = color;

    const customLabel = `
      <div>
        <strong>Country:</strong> ${country}<br>
        <div style='background-color: ${color};'>
        <strong>AQI:</strong> ${aqi} <i class='fa ${icon}'></i>
        ${message}
        </div>
      </div>
    `;

    const customIcon = AQIUtil.getCustomIcon();

    if (isAtTop) {
      const marker = L.marker([latitude, longitude], { icon: customIcon })
        .addTo(this.map)
        .bindPopup(customLabel)

      this.markers.push(marker);
    }
  }

  ngOnDestroy(): void {
    // Ensure the map is destroyed when the component is destroyed
    if (this.map) {
      this.map.remove();
    }
  }
}
