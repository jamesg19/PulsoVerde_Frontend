import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { ToastrService } from 'ngx-toastr';
import { AQIResponse, AqicnResponseDto } from 'src/app/models/aqi.model';
import { AQIService } from 'src/app/services/aqi.service';
import { AQIUtil } from 'src/app/util/aqi-util';
declare let $: any;

@Component({
  selector: 'app-tool-map',
  templateUrl: './tool-map.component.html',
  styleUrls: ['./tool-map.component.scss']
})
export class ToolMapComponent implements AfterViewInit, OnInit, OnDestroy {
  private map;
  searchLongitude = 39.8282;
  searchLatitude = -98.5795;
  longitude = 121.4489017;
  latitude = 31.2047372;
  date = "";
  aqi = 0;
  private markers: L.Marker[] = [];

  constructor(
    private toastService: ToastrService,
    private aqiService: AQIService
  ) {

  }

  ngAfterViewInit(): void {
    this.getLocation();
  }

  ngOnInit(): void {

  }

  getLocation() {
    if (this.searchLatitude && this.searchLongitude) {
      this.aqiService.specific(this.searchLatitude, this.searchLongitude).subscribe({
        next: (response) => {
          var aqi_response = response as AQIResponse;
          var aqi = aqi_response.original;
          this.longitude = aqi.data.city.geo[1];
          this.latitude = aqi.data.city.geo[0];
          this.aqi = aqi.data.aqi;
          this.date = aqi.data.time.s;
          if (!this.map) {
            this.initMap();
          }
          this.addCustomLabel();
          console.log(response);
        },
        error: (error) => {
          console.log(error);
        }
      })
    } else {
      this.toastService.error("Fill the two fields");
    }
  }


  private initMap(): void {
    this.map = L.map('map', {
      center: [this.latitude, this.longitude],
      zoom: 5
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  private addCustomLabel(): void {
    this.removeMarkers();
    const latitude = this.latitude;
    const longitude = this.longitude;
    const datetime = this.date;
    const aqi = this.aqi;
    var icon = "";
    var color = "";
    var message = "";
    if (aqi < 51) {
      icon = "fa-circle-check";
      color = "#00ff37";
      message = "ICA Bueno";
    } else if (aqi >= 51 && aqi < 101) {
      icon = "fa-thumbs-up";
      color = "#f9ff00";
      message = "ICA Moderado";
    } else if (aqi >= 101 && aqi < 151) {
      icon = "fa-triangle-exclamation";
      color = "#ffc100";
      message = "ICA Desfavorable para grupos sensibles";
    } else if (aqi >= 151 && aqi < 201) {
      icon = "fa-circle-radiation";
      color = "#ff0000";
      message = "ICA Dañino a la salud";
    } else {
      icon = "fa-skull";
      color = "#7f00ff";
      message = "ICA Muy dañino a la salud";
    }

    const customLabel = `
      <div>
        <strong>Latitude:</strong> ${latitude}<br>
        <strong>Longitude:</strong> ${longitude}<br>
        <strong>Time:</strong> ${datetime}<br>
        <div style='background-color: ${color};'>
        <strong>AQI:</strong> ${aqi} <i class='fa ${icon}'></i>
        ${message}
        </div>
      </div>
    `;

    const customIcon = AQIUtil.getCustomIcon();

    const marker = L.marker([latitude, longitude], { icon: customIcon })
      .addTo(this.map)
      .bindPopup(customLabel)
      .openPopup();

    this.markers.push(marker);
    this.map.panTo(new L.LatLng(latitude, longitude));
  }

  removeMarkers(): void {
    this.markers.forEach(marker => this.map.removeLayer(marker));
    this.markers = [];
  }

  ngOnDestroy(): void {
    // Ensure the map is destroyed when the component is destroyed
    if (this.map) {
      this.map.remove();
      $("#map-frame").html("");
      $("#map-frame").html('<div id="map"></div>');
    }
  }
}
