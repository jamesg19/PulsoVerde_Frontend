import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import * as L from 'leaflet';
import { ToastrService } from 'ngx-toastr';
import { AQIResponse, Iaqi } from 'src/app/models/aqi.model';
import { AQIService } from 'src/app/services/aqi.service';
import { AQIUtil } from 'src/app/util/aqi-util';
declare let $: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements AfterViewInit, OnInit, OnDestroy {
  private map;
  longitude = 39.8282;
  latitude = -98.5795;
  cities = [
    { name: 'Guatemala', id: '@502' },
    { name: 'Beijing', id: '@7397' },
    { name: 'New York', id: '@1234' },
    { name: 'London', id: '@5678' },
    { name: 'Tokyo', id: '@9101' },
    { name: 'Sydney', id: '@1121' },
    { name: 'Paris', id: '@3141' },
    { name: 'Berlin', id: '@5161' },
    { name: 'Moscow', id: '@7181' },
    { name: 'Dubai', id: '@9202' },
    { name: 'Singapore', id: '@2233' },
    { name: 'Hong Kong', id: '@4455' },
    { name: 'Rome', id: '@6677' },
    { name: 'Istanbul', id: '@8899' },
    { name: 'Madrid', id: '@1010' },
    { name: 'Los Angeles', id: '@1112' },
    { name: 'Chicago', id: '@1314' },
    { name: 'Mumbai', id: '@1516' },
    { name: 'Bangkok', id: '@1718' },
    { name: 'Seoul', id: '@1920' },
    { name: 'Buenos Aires', id: '@2122' },
    { name: 'Cairo', id: '@2324' },
    { name: 'São Paulo', id: '@2526' },
    { name: 'Toronto', id: '@2728' },
    { name: 'Mexico City', id: '@2930' },
    { name: 'Jakarta', id: '@3132' },
    { name: 'Lagos', id: '@3334' },
    { name: 'Kuala Lumpur', id: '@3536' },
    { name: 'Rio de Janeiro', id: '@3738' },
    { name: 'Cape Town', id: '@3940' },
    { name: 'Shanghai', id: '@4142' }
  ];
  countries = [
    "Afghanistan", "Albania", "Algeria", "Andorra", "Armenia", "Argentina",
    "Austria", "Azerbaijan", "Bahrain", "Bangladesh", "Belgium", "Belize",
    "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burundi",
    "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Central African Republic", "Chad", "Chile", "China", "Colombia",
    "Comoros", "Congo", "Costa Rica", "Croatia", "Cuba", "Cyprus", "Czechia", "Democratic Republic of the Congo",
    "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea",
    "Estonia", "Eswatini", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany",
    "Ghana", "Greece", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Holy See", "Honduras", "Hungary",
    "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan",
    "Kenya", "Kiribati", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein",
    "Lithuania", "Luxembourg", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania",
    "Mauritius", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia", "Montenegro", "Morocco", "Mozambique", "Myanmar",
    "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "North Korea", "North Macedonia",
    "Norway", "Oman", "Pakistan", "Palau", "Palestine State", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland",
    "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa",
    "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore", "Slovakia",
    "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Korea", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
    "Sweden", "Switzerland", "Syria", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste", "Togo", "Tonga", "Trinidad and Tobago",
    "Tunisia", "Turkey", "Turkmenistan", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "Estados Unidos",
    "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"
  ];
  iaqi: Iaqi = null;
  private aqi = null;
  private markers: L.Marker[] = [];
  date = "";
  city = "here";
  successfull = [];

  constructor(
    private toastService: ToastrService,
    private aqiService: AQIService
  ) {

  }

  ngOnInit(): void {
    
  }


  private initMap(): void {
    this.map = L.map('map', {
      center: [this.latitude, this.longitude],
      zoom: 5
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 1,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.getCity();
  }

  getCity() {
    if (this.city == "here") {
      this.aqiService.here().subscribe({
        next: (response) => {
          var aqi_response = response as AQIResponse;
          var aqi = aqi_response.original;
          this.iaqi = aqi.data.iaqi;
          this.aqi = aqi.data.aqi;
          this.date = aqi.data.time.s;
          this.longitude = aqi.data.city.geo[1];
          this.latitude = aqi.data.city.geo[0];
          if (!this.map) {
            this.initMap();
          }
          this.addCustomLabel();
        },
        error: (error) => {
          console.log(error);
        }
      })
    } else {
      this.aqiService.city(this.city).subscribe({
        next: (response) => {
          var aqi_response = response as AQIResponse;
          var aqi = aqi_response.original;
          this.iaqi = aqi.data.iaqi;
          this.aqi = aqi.data.aqi;
          this.date = aqi.data.time.s;
          this.longitude = aqi.data.city.geo[1];
          this.latitude = aqi.data.city.geo[0];
          if (!this.map) {
            this.initMap();
          }
          this.addCustomLabel();
        },
        error: (error) => {
          this.toastService.error("Unknown city!");
        }
      })
    }
  }

  private addCustomLabel(): void {
    this.removeMarkers();
    const latitude = this.latitude;
    const longitude = this.longitude;
    const datetime = this.date;
    const aqi = this.aqi;
    var icon = "";
    var color = "";
    var message = "";
    if (aqi < 51) {
      icon = "fa-circle-check";
      color = "#00ff37";
      message = "ICA Bueno";
    } else if (aqi >= 51 && aqi < 101) {
      icon = "fa-thumbs-up";
      color = "#f9ff00";
      message = "ICA Moderado";
    } else if (aqi >= 101 && aqi < 151) {
      icon = "fa-triangle-exclamation";
      color = "#ffc100";
      message = "ICA Desfavorable para grupos sensibles";
    } else if (aqi >= 151 && aqi < 201) {
      icon = "fa-circle-radiation";
      color = "#ff0000";
      message = "ICA Dañino a la salud";
    } else {
      icon = "fa-skull";
      color = "#7f00ff";
      message = "ICA Muy dañino a la salud";
    }

    const customLabel = `
      <div>
        <strong>Latitude:</strong> ${latitude}<br>
        <strong>Longitude:</strong> ${longitude}<br>
        <strong>DateTime:</strong> ${datetime}<br>
        <div style='background-color: ${color};'>
        <strong>AQI:</strong> ${aqi} <i class='fa ${icon}'></i>
        ${message}
        </div>
      </div>
    `;

    const customIcon = AQIUtil.getCustomIcon();

    const marker = L.marker([latitude, longitude], { icon: customIcon })
      .addTo(this.map)
      .bindPopup(customLabel)
      .openPopup();

    this.markers.push(marker);
    this.map.panTo(new L.LatLng(latitude, longitude));
  }

  removeMarkers(): void {
    this.markers.forEach(marker => this.map.removeLayer(marker));
    this.markers = [];
  }

  ngOnDestroy(): void {
    // Ensure the map is destroyed when the component is destroyed
    if (this.map) {
      this.map.remove();
    }
  }
}
