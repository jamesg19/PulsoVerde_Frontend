import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ToastrService} from "ngx-toastr";
import { ActivityEcoService } from 'src/app/services/activityeco.service';
import {  ActivityEcoUpRequest, ChangeActivityRequest} from 'src/app/models/activity.model';
import { HttpErrorResponse } from '@angular/common/http';
import {AuthService} from 'src/app/services/auth.service';


@Component({
  selector: 'app-activity-eco-crud',
  templateUrl: './activity-eco-crud.component.html',
  styleUrls: ['./activity-eco-crud.component.scss']
})
export class ActivityEcoCrudComponent implements OnInit {

  ActivityUp: ActivityEcoUpRequest = new ActivityEcoUpRequest();

  listActive: ActivityEcoUpRequest[]=[];
  listExpired: ActivityEcoUpRequest[]=[];//any= null;

  newActivity=true;
  idActivity='-1';
  token="";

  constructor(
    private toastService: ToastrService,
    private activityEcoService: ActivityEcoService,
    private router: Router,
    private route: ActivatedRoute,
    private auth:AuthService
  ){


  }

  ngOnInit(): void {
    this.token=this.auth.getToken();
      this.loadlist();
  }

   loadlist(){
     this.activityEcoService.getActivityInfo('true').subscribe({
      next: (response: any) => {
         this.listActive=response;
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
      }
    });

     this.activityEcoService.getActivityInfo('false').subscribe({
      next: (response: any) => {
        this.listExpired=response;
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
      }
    });

    
    
  }

  clearcamp(){
    this.ActivityUp= new ActivityEcoUpRequest(); 
  }

  newActivityUp() {   
  if(this.validform()){
    this.activityEcoService.setactivityIfo(this.ActivityUp).subscribe({
      next: (response: any) => {
        this.toastService.success("Activity created!")
        this.loadlist();
        this.clearcamp();

      },
      error: (error: HttpErrorResponse) => {
        
      }
    })
  }else{
    this.toastService.error("fill out all fields!");

  }
  }


  updateActivityUp(state:boolean) {   

     if(this.validform() || state){


      this.activityEcoService.updateactivityIfo( this.ActivityUp,this.token,this.idActivity).subscribe({
        next: (response: any) => {
          this.toastService.success("Activity Update!")
          this.loadlist();
          this.clearcamp();

        },
        error: (error: HttpErrorResponse) => {
        }
      })
     }else{
       this.toastService.error("fill out all fields!");
  
     }
    }

  validform() {
    return (this.ActivityUp.name!=null && this.ActivityUp.name!="") && 
    (this.ActivityUp.description!=null && this.ActivityUp.description!="") &&
    (this.ActivityUp.publicationExpired!=null && this.ActivityUp.publicationExpired!="") &&
    (this.ActivityUp.activityPhoto!=null && this.ActivityUp.activityPhoto!="");
  }

  onFileChange(event: Event): void {
    const input = event.target as HTMLInputElement;

    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      const reader = new FileReader();

      reader.onload = () => {
        this.ActivityUp.activityPhoto = reader.result as string;
      };

      reader.readAsDataURL(file);
    }
  }

  Edit(activityedit:ActivityEcoUpRequest,idActivity: number){
    this.newActivity=false;
    this.idActivity=idActivity.toString();
    this.ActivityUp=activityedit;
    this.scrollToTop();
  }
  onSwitchChange(activityedit:ActivityEcoUpRequest,idActivity: number){
    this.newActivity=false;
    this.idActivity=idActivity.toString();
    this.ActivityUp=activityedit;
    this.updateActivityUp(true);

  }
  scrollToTop(): void {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

}
