import {Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ToastrService} from "ngx-toastr";
import { ActivityEcoService } from 'src/app/services/activityeco.service';
import {  AssignedActivityRequest,ActivityEcoUpRequest} from 'src/app/models/activity.model';
import { HttpErrorResponse } from '@angular/common/http';
import {AuthService} from 'src/app/services/auth.service';


@Component({
  selector: 'app-activity-eco',
  templateUrl: './activity-eco.component.html',
  styleUrls: ['./activity-eco.component.scss']
})
export class ActivityEcoComponent implements OnInit {

  ActivityUp: AssignedActivityRequest = new AssignedActivityRequest();
  ActivityUpInac: ActivityEcoUpRequest = new ActivityEcoUpRequest();

  listActive: AssignedActivityRequest[]=[];
  listExpired: ActivityEcoUpRequest[]=[];//any= null;
  listUnAssigned: ActivityEcoUpRequest[]=[];//any= null;
  listaid: ActivityEcoUpRequest[] =[];
  newActivity=true;
  idActivity='-1';
  token="";
  isActive=true;

  constructor(
    private toastService: ToastrService,
    private activityEcoService: ActivityEcoService,
    private router: Router,
    private route: ActivatedRoute,
    private auth:AuthService
  ){


  }

  ngOnInit(): void {
    this.token=this.auth.getToken();
      this.loadlist();
  }

   loadlist(){



     this.activityEcoService.getActivityAssignInfo().subscribe({
      next: (response: any) => {

         this.listActive=response;
        
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
      }
    });

     this.activityEcoService.getActivityUnAssignInfo().subscribe({
      next: (response: any) => {
        this.listUnAssigned=response;
      },
      error: (error: HttpErrorResponse) => {
        console.log(error);
      }
    });


    
  }

  clearcamp(){
    this.ActivityUp= new AssignedActivityRequest(); 
  }


  AssignActivityUp() {   
    console.log('AssignActivityUp',this.idActivity);
      this.activityEcoService.AsignateactivityIfo(this.token,this.idActivity).subscribe({
        next: (response: any) => {
            this.toastService.success("correct Assigned activity !")

          this.loadlist();

        },
        error: (error: HttpErrorResponse) => {
        }
      })

    }

    UnassignActivityUp() {   
      console.log('UnassignActivityUp',this.token,this.idActivity);

      this.activityEcoService.unAsignateactivityIfo(this.token,this.idActivity).subscribe({
        next: (response: any) => {

            this.toastService.success("Unallocated activity !")
          this.loadlist();

        },
        error: (error: HttpErrorResponse) => {
        }
      })

    }

    




  onSwitchChange(activityedit:AssignedActivityRequest){

    this.idActivity=activityedit.activity.id.toString();
    this.ActivityUp=activityedit;

      this.UnassignActivityUp();


  }

  onSwitchChangeInact(activityedit:ActivityEcoUpRequest){

    this.idActivity=activityedit.id.toString();
    this.ActivityUpInac=activityedit;
    this.AssignActivityUp();



  }

  scrollToTop(): void {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

}
