import {Component, Inject} from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {UserDto} from "../../../../Objects/UserDto";
import {ProfileComponent} from "../profile.component";
@Component({
  selector: 'app-view-other',
  templateUrl: './view-other.component.html',
  styleUrls: ['./view-other.component.scss']
})
export class ViewOtherComponent {
  constructor(
    public dialogRef: MatDialogRef<ProfileComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
  }
  onClose(): void {
    this.dialogRef.close();
  }

}
