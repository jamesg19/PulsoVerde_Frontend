import {Component, Input} from '@angular/core';
import { ProfileService } from 'src/app/services/profile.service';
import {CountryService} from "../../../services/country.service";
import {UserDto} from "../../../Objects/UserDto";
import {CountryDto} from "../../../Objects/CountryDto";


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  user: UserDto | undefined;
  countries: CountryDto[] = [];
  isEditing: boolean = false;
  errorMessage: string | null = null;
  @Input() seeOther:boolean = false;
  @Input() idUser:number;
  constructor(private userService: ProfileService, private countryService: CountryService) { }

  ngOnInit(): void {
    if(this.seeOther){
      this.userService.seeOtherUser(this.idUser).subscribe(
        (data) => {
          this.user = data;
        },
        (error) => {
          console.error('Error fetching user data', error);
        }
      );
    }else{
      this.userService.getUserInfo().subscribe(
        (data) => {
          this.user = data;
          this.assignCountry();
          console.log(this.user);
        },
        (error) => {
          console.error('Error fetching user data', error);
        }
      );
    }
  }
  getCountryList (){
    this.countryService.getCountry().subscribe(
      (data) => {
        this.countries = data;
        this.assignCountry();
      },
      (error) => {
        console.error('Error fetching countries', error);
      }
    );
  }

  getProfilePhoto(): string {
    return this.user?.profilePhoto ? `${this.user.profilePhoto}` : 'assets/img/perfil.png';
  }
  toggleEdit(): void {
    this.isEditing = !this.isEditing;
    if (this.isEditing) {this.getCountryList();}
  }
  private assignCountry(): void {
    if (this.user && this.countries.length > 0) {
      const userCountry = this.countries.find(country => country.countryId === this.user!.country.countryId);
      if (userCountry) {
        this.user!.country = userCountry;
      }
    }
  }

  saveProfile(): void {
    // Lógica para guardar el perfil actualizado
    console.log(this.user);
    //this.isEditing = false;
    if (this.user) {
      this.errorMessage = this.validateUser(this.user);
      if (this.errorMessage) {
        alert(this.errorMessage);
        return;
      }
      this.userService.updateUser(this.user).subscribe(
        (updatedUser) => {
          this.user = updatedUser;
          console.log('Updated user data:', this.user);
          this.isEditing = false;
        },
        (error) => {
          console.error('Error updating user data', error);
        }
      );
    }
  }


  validateUser(user: UserDto): string | null {
    if (!user.firstName) return 'First Name cannot be null';
    if (!user.lastName) return 'Last Name cannot be null';
    if (!user.username) return 'Username cannot be null';
    if (!user.country || !user.country.name) return 'Country cannot be null';
    if (!user.birthday) return 'Birthday cannot be null';
    if (!user.age) return 'Age cannot be null';
    if (!user.description) return 'Description cannot be null';
    return null;
  }

  onFileSelected(event: any): void {
    const input = event.target as HTMLInputElement;

    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      const reader = new FileReader();

      reader.onload = () => {
        this.user.profilePhoto = reader.result as string;
      };

      reader.readAsDataURL(file);
    }
  }

}
