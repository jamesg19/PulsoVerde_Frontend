import { Component, OnInit } from '@angular/core';
import { UserDto } from "../../../Objects/UserDto";
import { UserService } from "../../../services/user.service";
import { ToastrService } from "ngx-toastr";
import { PaginationModel } from 'src/app/models/pagination.model';
import { CountryFlags } from 'src/app/util/country-flags';
import { MatDialog } from '@angular/material/dialog';
import {ViewOtherComponent} from "../profile/view-other/view-other.component";

@Component({
  selector: 'app-users-search',
  templateUrl: './users-search.component.html',
  styleUrls: ['./users-search.component.scss']
})
export class UsersSearchComponent implements OnInit {
  pagination = new PaginationModel(1, 4, 0, 0, 0);
  flags = new CountryFlags();
  textFilter: string = "";
  params = new Map<string, any>;
  typingTimer: any;
  typingInterval = 750;
  totalItems: any = null;

  users: UserDto[] = [];

  constructor(
    private userService: UserService,
    private toasterService: ToastrService,
    public dialog : MatDialog
  ) {
  }

  ngOnInit(): void {
    this.findWithPagination();
  }

  findAll() {
    this.params.set("user", this.textFilter);
    this.userService.findByQuery(this.params).subscribe({
      next: (entities) => {
        this.users = entities ?? [];
      }, error: _ => this.toasterService.error("Error en el servidor!")
    });
  }

  onKeyUp() {
    clearTimeout(this.typingTimer);
    this.typingTimer = setTimeout(() => this.findWithPagination(), this.typingInterval);
  }

  findWithPagination() {
    this.userService.findByQueryAndPagination(this.textFilter, this.pagination.pageSize, this.pagination.pageIndex - 1).subscribe(
      (response) => {
        this.users = response.body;
        this.totalItems = parseInt(response.headers.get('Total-Items')); // Access Total-Items header
        this.pagination.startElement = (this.pagination.pageIndex - 1) * this.pagination.pageSize + (this.totalItems > 0 ? 1 : 0);
        this.pagination.lastElement = this.pagination.startElement + (this.users.length - (this.totalItems > 0 ? 1 : 0));
        this.pagination.total = this.totalItems;
      },
      (error) => {
        console.error('Error fetching data:', error);
      }
    );
  }

  changePagination() {
    this.findWithPagination();
  }

  openDialog(user: UserDto): void {
    const dialogRef = this.dialog.open(ViewOtherComponent, {
      width: '80vw',  // Ajusta según sea necesario
      height: '80vh',
      data: { user }
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('El diálogo se cerró');
    });
  }
}
