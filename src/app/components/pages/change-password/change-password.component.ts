import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ChangePassRequest } from 'src/app/models/auth.model';
import { ButtonLoading } from 'src/app/models/button-loading.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss', './../auth/login/login.component.scss']
})
export class ChangePasswordComponent {
  password1 = "";
  password2 = "";
  loading = false;
  buttonChange = new ButtonLoading("btn-success", false, "CHANGE PASSWORD", "fa-save");
  token = "";

  constructor(
    private toastService: ToastrService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.route.paramMap.subscribe({
      next: params => {
        this.token = params.get('token');
      }
    });

  }

  changePassword() {
    if (this.buttonChange.loading) {
      return;
    }
    this.buttonChange.loading = true;
    var request = new ChangePassRequest();
    request.token = this.token;
    request.newPassword = this.password1;
    this.authService.changePass(request).subscribe({
      next: (response) => {
        this.toastService.success("Password changed successfully!");
        this.buttonChange.loading = false;
        this.router.navigate(['auth/login']);
      },
      error: (error) => {
        this.buttonChange.loading = false;
        this.toastService.error("The password is not changed");
      }
    })
  }
}
