import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthResponse, SignUpRequest } from 'src/app/models/auth.model';
import { ButtonLoading } from 'src/app/models/button-loading.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss', '../login/login.component.scss']
})
export class RegisterComponent {
  signUp: SignUpRequest = new SignUpRequest();
  loading = false;
  buttonRegister: ButtonLoading = new ButtonLoading("btn-success", false, "SIGN UP", "fa-save");
  password1 = "";
  password2 = "";

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastService: ToastrService
  ) {
  }

  newSignUp() {
    if (this.buttonRegister.loading){
      return;
    }
    this.buttonRegister.loading = true;
    this.signUp.password = this.password1;
    this.authService.signUp(this.signUp).subscribe({
      next: (response: AuthResponse) => {
        this.toastService.success("User created!")
        this.router.navigate(['auth/login']);
        this.buttonRegister.loading = false;
      },
      error: (error: HttpErrorResponse) => {
        this.buttonRegister.loading = false;
      }
    })
  }

  onFileChange(event: Event): void {
    const input = event.target as HTMLInputElement;

    if (input.files && input.files.length > 0) {
      const file = input.files[0];
      const reader = new FileReader();

      reader.onload = () => {
        this.signUp.profilePhoto = reader.result as string;
      };

      reader.readAsDataURL(file);
    }
  }
}
