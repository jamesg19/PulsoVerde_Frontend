import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthRequest, AuthResponse } from 'src/app/models/auth.model';
import { ButtonLoading } from 'src/app/models/button-loading.model';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  newLogin: AuthRequest = new AuthRequest();
  loading = false;
  buttonLogin: ButtonLoading = new ButtonLoading("btn-success", false, "SIGN IN", "fa-right-to-bracket");


  constructor(
    private authService: AuthService,
    private router: Router,
    private toastService: ToastrService,
  ) {  }

  login() {
    if (this.buttonLogin.loading){
      return;
    }
    this.buttonLogin.loading = true;
    this.authService.login(this.newLogin).subscribe({
      next: (response: AuthResponse) => {
        this.authService.setToken(response.jwt);
        this.buttonLogin.loading = false;
        this.router.navigate(['aqi/map']);
      },
      error: (error: HttpErrorResponse) => {
        this.toastService.error("Credenciales invalidas");
        this.buttonLogin.loading = false;
      }
    })
  }
}
