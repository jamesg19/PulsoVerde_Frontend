import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaginationModel } from 'src/app/models/pagination.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Input() elements: any = new PaginationModel(1, 4, 0, 0, 0);
  @Output() changed = new EventEmitter<any[]>();

  // Al realizar modificaciones

  constructor() {
  }

  ngOnInit(): void {

  }

  start() {
    this.elements.pageIndex = 1;
    this.changed.emit(this.elements);
  }

  getMaxPage() {
    var max = Math.ceil(this.elements.total / this.elements.pageSize);
    if (max <= 0) {
      max = 1;
    }
    return max;
  }

  changePageIndex() {
    var max = Math.ceil(this.elements.total / this.elements.pageSize);
    if (this.elements.pageIndex > max) {
      this.elements.pageIndex = max;
    }
    if (this.elements.pageIndex <= 0) {
      this.elements.pageIndex = 1;
    }
    this.changed.emit();
  }

  last() {
    this.elements.pageIndex = Math.ceil(this.elements.total / this.elements.pageSize);
    this.changed.emit(this.elements);
  }

  previous() {
    this.elements.pageIndex--;
    this.changed.emit(this.elements);
  }

  next() {
    this.elements.pageIndex++;
    this.changed.emit(this.elements);
  }

  changePageSize() {
    this.changed.emit(this.elements);
  }

  isOnLastPage() {
    if (this.elements.total == 0) {
      return true;
    } else {
      return Math.ceil(this.elements.total / this.elements.pageSize) == this.elements.pageIndex;
    }
  }

  refresh() {
    this.elements.startElement = this.elements.pageIndex * this.elements.pageSize;
    var temporalLast = this.elements.startElement + this.elements.pageSize;
    this.elements.lastElement = (temporalLast < this.elements.total.length) ? temporalLast : this.elements.total.length;
    this.elements.filter = this.elements.total.slice(this.elements.startElement, this.elements.lastElement);
  }

  reset() {
    this.elements.startElement = 0;
    this.elements.pageIndex = 0;
    this.elements.lastElement = this.elements.pageSize;
    this.elements.filter = this.elements.total.slice(this.elements.startElement, this.elements.lastElement);
    this.elements.lastElement = this.elements.filter.length;
    this.changed.emit(this.elements);
  }

}
