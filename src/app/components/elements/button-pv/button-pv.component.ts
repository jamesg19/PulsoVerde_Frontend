import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button-pv',
  templateUrl: './button-pv.component.html',
  styleUrls: ['./button-pv.component.scss']
})
export class ButtonpvComponent {

  @Input() text: string = 'texto button';
  @Input() typebutton: string='primary';
  @Input() icon: string ="";
  @Input() customBtnClick: any;
  @Input() parameterArray: Array<any> = [];
  @Output() focus = new EventEmitter(false);

  EjecutarAccion() {
    console.log('se ejecuta el boton');
    if (this.customBtnClick) {
      if ( this.customBtnClick instanceof EventEmitter ) {
        this.customBtnClick.emit(...this.parameterArray);
      }
      else {
        this.customBtnClick(...this.parameterArray);
      }
    }
    else {
      console.error("Custom button click event not defined. firing focus!");
    }
    this.focus.emit();
  }

}
