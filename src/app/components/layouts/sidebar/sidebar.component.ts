import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd,ActivatedRoute } from "@angular/router";
import { RoleEnum } from 'src/app/enums/role.enum';
import {AuthService} from 'src/app/services/auth.service';

declare let $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  showElement = false;
  token="";
  role=0;
  roleEnum = RoleEnum;
  rutacrud="";
  constructor(private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService
  ) {
    router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        $(".responsive-burger-menu").removeClass("active");
        $(".sidemenu-area").removeClass("active-sidemenu-area");
      }
    });

  
  }

  ngOnInit() {
    this.token=this.auth.getToken();
    this.role=this.auth.getRol();
    console.log('este es el rol',this.role);
    // Burger Menu JS
    $(".responsive-burger-menu").on("click", function () {
      $(".responsive-burger-menu").toggleClass("active");
      $(".sidemenu-area").toggleClass("active-sidemenu-area");
    });
  }
}
