
export class ActivityEcoDto {

  id: number;
	name: string;
	description: string;
	state: boolean;
	publicationExpired: any;
	publicationDate: any;
  user:any;
	activityPhoto: string;
  

  constructor(data: any) {
    this.id = data.id;
    this.name = data.name;
    this.description = data.description;
    this.state=data.state;
    this.publicationExpired = data.publicationExpired;
    this.publicationDate = data.publicationDate;
    this.user=data.user;
    this.activityPhoto= data.activityPhoto;
    


  }
}
