export class CountryDto {
  countryId: number;
  code: string;
  name: string;

  constructor(data: any) {
    this.countryId = data.countryId;
    this.code = data.code;
    this.name = data.name;
  }
}
