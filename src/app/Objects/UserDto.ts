import {CountryDto} from "./CountryDto";


export class UserDto {
  userId: number;
  email: string;
  fullName: string;
  firstName: string;
  lastName: string;
  birthday: string;
  age: number;
  country: CountryDto;
  description: string;
  profilePhoto: string | null;
  username: string;
  points:number;

  constructor(data: any) {
    this.userId = data.userId;
    this.email = data.email;
    this.fullName = data.fullName;
    this.birthday = data.birthday;
    this.age = data.age;
    this.country = new CountryDto(data.country);
    this.description = data.description;
    this.profilePhoto = data.profilePhoto;
    this.username = data.username;
    this.points = data.points;
  }
}
